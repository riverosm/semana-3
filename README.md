**Instalar dependencias**

1. npm install bootstrap --save
2. npm install jquery --save
3. npm install popper.js --save

**Te invitamos a poner a prueba tus aprendizajes adquiridos hasta ahora en el curso. Se trata del producto resultante de llevar a cabo las guías prácticas del Módulo 3:**

1. *Crear comportamiento dinámico utilizando Javascript*

2. *Crear elementos de navegación utilizando pestañas y menús desplegables*

3. *Crear alertas y vistas modales y un carousel.*

**En tu proyecto deberá verse:**

    tabs o pills en alguna sección del sitio. Por ejemplo, tabla comparativa, opciones de pago, información para la compra, etc. y su versión responsive se ve correctamente
    secciones que muestran información utilizando el componente collapse y multi collapse.
    el componente accordion en alguna sección donde muestre un listado de opciones (p. ej. opciones de pago con tarjetas en la sección de precios).
    información de contexto via tooltips o popovers en los títulos de los productos mostrados en las cards.
    un componente modal sobre el final del body que invite al usuario a dejar sus datos o a registrarse.
    un formulario para que el usuario complete en el cuerpo del modal.
    un botón que abra el modal.
    un componente carousel en la parte superior de la página principal, que muestre algo merecedor de notoriedad.
    controles de desplazamiento sobre el carousel.
    un script que modifique la velocidad de desplazamiento a tu criterio. 

Recuerda que esta actividad será evaluada por tus pares y se espera que también lo hagas tú. Por ello, es muy importante que evalúes a conciencia, pensando en que tantos tus compañeros como vos, están queriendo aprender en este curso. Guíate por los criterios de corrección que te orientarán en todo el proceso de evaluación.

**¡Éxitos!**